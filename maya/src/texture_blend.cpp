
#include <maya/MIOStream.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnEnumAttribute.h>


#include <maya/MGlobal.h>
#include <maya/MItGeometry.h>
#include <maya/MItMeshVertex.h>
#include <maya/MFnComponentListData.h>
#include <maya/MFnSingleIndexedComponent.h>
#include <maya/MFnMesh.h>
#include <maya/MRenderUtil.h>
#include <maya/MPlugArray.h>
#include <maya/MFloatArray.h>
#include <maya/MVector.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatPoint.h>
#include <maya/MPoint.h>
#include <maya/MFloatPointArray.h>
#include <maya/MPointArray.h>
#include <maya/MFloatMatrix.h>
#include <maya/MDagPath.h>
#include <maya/MMatrix.h>
#include <maya/MTypeId.h>
#include <maya/MDataBlock.h>
#include "errorMacros.h"
#include "attrUtils.h"
#include "jMayaIds.h"

#include "texture_blend.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

MTypeId	textureBlend::id( k_textureBlend );
MObject	textureBlend::aTarget;
MObject	textureBlend::aBlendValue;
MObject	textureBlend::aTargetSpace;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

textureBlend::textureBlend() {}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

textureBlend::~textureBlend() {}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void* textureBlend::creator() {
	return new textureBlend();
}




///////////////////////////////////////////////////////////////////////////////////////////////////////////////

MStatus textureBlend::initialize() {


	MStatus st;
	MFnNumericAttribute		nAttr;
	MFnTypedAttribute		tAttr;
	MFnEnumAttribute		eAttr;

    aTarget= tAttr.create( "target", "tg",MFnData::kMesh,&st);er;
    tAttr.setReadable(false);
    tAttr.setReadable( true );
    tAttr.setArray(true );
    tAttr.setIndexMatters(true );
   	st = addAttribute( aTarget );er;

	aBlendValue = nAttr.create( "blendValue", "bv", MFnNumericData::kFloat);
	nAttr.setStorable( true );
	nAttr.setKeyable( true );
	nAttr.setReadable( true );
	nAttr.setWritable( true );
	st = addAttribute( aBlendValue );er;




	aTargetSpace = eAttr.create("targetSpace", "tgs");
	eAttr.addField("object", textureBlend::kObject);
	eAttr.addField("world", textureBlend::kWorld);
	eAttr.setDefault(textureBlend::kWorld);
	eAttr.setKeyable(true);
	eAttr.setWritable(true);
	st = addAttribute(aTargetSpace );er;
	st = attributeAffects(aTargetSpace, outputGeom ); er;
	st = attributeAffects(aTarget, outputGeom ); er;
	st = attributeAffects(aBlendValue, outputGeom ); er;

	return MStatus::kSuccess;
}

MStatus textureBlend::getTextureName(const MObject &attribute, MString &name) const {
	name = "";
	MStatus st;
	MPlugArray plugArray;
	MPlug plug(thisMObject(), attribute);
	bool hasConnection = plug.connectedTo(plugArray,1,0,&st);ert;
	if (! hasConnection) return MS::kUnknownParameter;
	name = plugArray[0].name(&st);
	return MS::kSuccess;
}

MStatus textureBlend::sampleTexture(
	const MString & sourceName,
	MFloatPointArray & pVals,
	MFloatArray &uVals ,
	MFloatArray &vVals , 
	MFloatVectorArray & nVals,
	MFloatArray & result
) const {
	MStatus st;
	unsigned n = pVals.length();
	result.setLength(n);
	MFloatMatrix cameraMat;
	cameraMat.setToIdentity();
	MFloatVectorArray transparencies;
	MFloatVectorArray colors;
	st =  MRenderUtil::sampleShadingNetwork (
		sourceName,n,false,false,
		cameraMat,&pVals,&uVals,&vVals,&nVals,&pVals,
		0,0,0,colors,transparencies
	);ert;
	for (unsigned i = 0; i < n; ++i)
	{
		result.set(colors[i].x,i);
	}
	return MS::kSuccess;
}


MStatus textureBlend::componentWeights(
	const MString &mapName,
	MItMeshVertex & vIter,
	float defaultWeight,
	MFloatArray &result
) const {
	MStatus st;

	unsigned n = vIter.count();


	if (mapName == "") {
		result.setLength(n);
		for (unsigned i = 0; i < n; ++i)
		{
			result.set(defaultWeight, i);
		}
	} else {

		MFloatPointArray pVals;
		MFloatArray uVals;
		MFloatArray vVals;
		MFloatVectorArray nVals;

		for (vIter.reset(); !vIter.isDone(); vIter.next() )
		{
			MPoint p = vIter.position(MSpace::kWorld, &st);ert
			pVals.append(MFloatPoint(p));
			MVector norm;
			st = vIter.getNormal(norm, MSpace::kWorld);
			nVals.append(MFloatVector(norm));
			float2 uv;
			vIter.getUV(uv);
			uVals.append(uv[0]);
			vVals.append(uv[1]);
		}
		st = sampleTexture(mapName, pVals, uVals , vVals , nVals, result);ert;

	}
	// JPMDBG;
	return MS::kSuccess;
}


MStatus textureBlend::deform( MDataBlock& data, MItGeometry& iter, const MMatrix& m, unsigned int multiIndex) {

	MStatus st;

	MObject thisNode = thisMObject();

	MPlug inPlug(thisNode, input);
	inPlug.selectAncestorLogicalIndex(multiIndex,input);

	MDataHandle hInput = data.inputValue(inPlug);
	MDataHandle hGeom = hInput.child(inputGeom); 

	TargetSpace tSpace = TargetSpace(data.inputValue(aTargetSpace).asShort());


	MObject dGeom = hGeom.data();
	if (! dGeom.hasFn(MFn::kMesh)) {
		//MGlobal::displayWarning("TextureBlend currently works only on meshes");
		return MS::kUnknownParameter;
	}

	MItMeshVertex vIter(dGeom, &st);
	int vc = vIter.count();

	if (iter.count() != vc) {
	 	MGlobal::displayWarning("TextureBlend currently works only on complete meshes");
		return MS::kUnknownParameter;
	}

	MArrayDataHandle ah = data.inputArrayValue(aTarget);
	st = ah.jumpToElement(multiIndex); ert;
	MDataHandle h = ah.inputValue(&st);ert;
	MObject dTarget =  h.asMesh();
	MFnMesh fnTarget(dTarget);
	if (iter.count() != fnTarget.numVertices()) {
		MGlobal::displayWarning("TextureBlend needs source and targets with the same topology");
		return MS::kUnknownParameter;
	}

	float env = data.inputValue(envelope).asFloat();
	if (env == 0.0f) 	return MS::kSuccess;

	MFloatArray weights;

	float defaultValue = data.inputValue(aBlendValue).asFloat();

	MString name = "";
	st = getTextureName(aBlendValue, name);

	st = componentWeights(name, vIter, defaultValue, weights);
	// cerr << "Num Verts: " << weights.length() << endl;
	MPointArray points;

	MPointArray targetPoints;
	if (tSpace == textureBlend::kWorld) {
		st = fnTarget.getPoints(targetPoints,MSpace::kWorld);ert;
		//unsigned tl = targetPoints.length();
		unsigned i = 0;
		for (iter.reset(); !iter.isDone(); iter.next(),i++ ) {
			float w =  weights[i] * env;
			MPoint pt = iter.position() * m;
			pt = (pt* (1.0 - w)) + (targetPoints[i] * w);
			pt = pt  * m.inverse();	
			iter.setPosition(pt);
		}
	} else {
		st = fnTarget.getPoints(targetPoints,MSpace::kObject);ert;
		//unsigned tl = targetPoints.length();
		unsigned i = 0;
		for (iter.reset(); !iter.isDone(); iter.next(),i++ ) {
			float w =  weights[i] * env;
			MPoint pt = iter.position() * m;
			pt = (pt* (1.0 - w)) + ((targetPoints[i] * m) * w);
			pt = pt  * m.inverse();	
			iter.setPosition(pt);
		}
	}

	return st;
}

