/*
 *  wakeDeformer.h
 *  jtools
 *
 *  Created by Julian Mann on 30/07/2008.
 *  Copyright 2008 hoolyMama. All rights reserved.
 *
 */


#ifndef _wakeDeformer
#define _wakeDeformer


#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MTypeId.h>

#include <maya/MGlobal.h>
#include <maya/MItGeometry.h>
#include <maya/MMatrix.h>
#include <maya/MPxDeformerNode.h>
#include <lineTreeData.h>

////////////////////////////////////////////////////////////
class wakeDeformer : public MPxDeformerNode {
	
public:
	wakeDeformer();
	virtual							~wakeDeformer();
	
	static		void*		creator();
	static		MStatus		initialize();
	
	virtual		MStatus		deform(	MDataBlock& block,
								   MItGeometry& iter,
								   const MMatrix& mat,
								   unsigned int multiIndex);
	
	void doRampLookup( const MObject& attribute, const MFloatArray& in, float mult , MFloatArray& results) const;
	MStatus normalizeDistances(const MFloatArray& numerators, const MFloatArray& denominators,  MFloatArray& results) const;
public:
	static		MObject		aCurve;
	static		MObject		aMaxDistance;
	
	static		MObject		aVector;
	static		MObject		aNormal;
	static		MObject		aTangent;
	static		MObject		aOrbital;

	static		MObject		aVectorFalloff;
	static		MObject		aNormalFalloff;
	static		MObject		aTangentFalloff;
	static		MObject		aOrbitalFalloff;
	
	static		MObject		aVectorParamDistance;
	static		MObject		aNormalParamDistance;
	static		MObject		aTangentParamDistance;
	static		MObject		aOrbitalParamDistance;
	
	static		MObject		aVectorParamMag;
	static		MObject		aNormalParamMag;
	static		MObject		aTangentParamMag;
	static		MObject		aOrbitalParamMag;
	
	static		MTypeId		id;
	
};

#endif