
#include <maya/MIOStream.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MGlobal.h>
#include <maya/MItGeometry.h>
#include <maya/MItMeshVertex.h>
#include <maya/MFnMesh.h>
#include <maya/MRenderUtil.h>
#include <maya/MPlugArray.h>
#include <maya/MFloatArray.h>
#include <maya/MVector.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatPoint.h>
#include <maya/MPoint.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatMatrix.h>
#include <maya/MDagPath.h>
#include <maya/MMatrix.h>
#include <maya/MTypeId.h>
#include <maya/MDataBlock.h>
#include "errorMacros.h"
#include "attrUtils.h"
#include "jMayaIds.h"

#include "textureDeformer.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

MTypeId	textureDeformer::id( k_textureDeformer );

MObject	textureDeformer::aSource;
MObject	textureDeformer::aDirection;
MObject	textureDeformer::aGradient;

MObject	textureDeformer::aDirectionMethod;

MObject	textureDeformer::aSampleDistance;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

textureDeformer::textureDeformer() {}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

textureDeformer::~textureDeformer() {}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void* textureDeformer::creator() {
	return new textureDeformer();
}




///////////////////////////////////////////////////////////////////////////////////////////////////////////////

MStatus textureDeformer::initialize() {


	MStatus st;
	MFnNumericAttribute		nAttr;
	//MFnTypedAttribute		tAttr;
	// MFnMessageAttribute	mAttr;
	MFnEnumAttribute		eAttr;
	//MFnCompoundAttribute	cAttr;
	//MRampAttribute			rAttr;


	aSampleDistance= nAttr.create( "sampleDistance", "sd", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	nAttr.setConnectable(true);
	nAttr.setKeyable(true);
	addAttribute( aSampleDistance );




	aSource = nAttr.create( "value", "val", MFnNumericData::kFloat);
	nAttr.setStorable( true );
	nAttr.setKeyable( true );
	nAttr.setReadable( true );
	nAttr.setWritable( true );
	nAttr.setKeyable( true );
	addAttribute( aSource );

	aDirection = nAttr.createColor("direction","dir");
	nAttr.setStorable( true );
	nAttr.setKeyable( true );
	nAttr.setReadable( true );
	nAttr.setWritable( true );
	nAttr.setDefault( 0.0, 1.0, 0.0 );
	st = addAttribute( aDirection );er;


	aGradient = nAttr.create( "gradient", "grd", MFnNumericData::kFloat);
	nAttr.setStorable( true );
	nAttr.setKeyable( true );
	nAttr.setReadable( true );
	nAttr.setWritable( true );
	nAttr.setKeyable( true );
	addAttribute( aGradient );



	aDirectionMethod = eAttr.create("directionMethod", "dmt");
	eAttr.addField("direction", textureDeformer::kDirection);
	eAttr.addField("normal", textureDeformer::kNormal);
	eAttr.addField("gradientOnly", textureDeformer::kGradient);
	eAttr.addField("gradientByValue", textureDeformer::kGradientBySource);
	
	eAttr.setDefault(textureDeformer::kNormal);
	eAttr.setKeyable(true);
	eAttr.setWritable(true);
	st = addAttribute(aDirectionMethod );er;

	st = attributeAffects(aSource, outputGeom ); er;
	st = attributeAffects(aSampleDistance, outputGeom ); er;
	st = attributeAffects(aDirection, outputGeom ); er;
	st = attributeAffects(aGradient, outputGeom ); er;
	
	st = attributeAffects(aDirectionMethod, outputGeom ); er;

	return MStatus::kSuccess;
}

MStatus textureDeformer::getTextureName(const MObject &attribute, MString &name) {
	MStatus st;
	MPlugArray plugArray;
	MPlug plug(thisMObject(), attribute);
	bool hasConnection = plug.connectedTo(plugArray,1,0,&st);
	if (st.error()) return st;
	if (! hasConnection) return MS::kUnknownParameter;
	name = plugArray[0].name(&st);
	return MS::kSuccess;
}

MStatus textureDeformer::sampleTexture(
	const MString & sourceName,
	MFloatPointArray & pVals,
	MFloatArray &uVals ,
	MFloatArray &vVals , 
	MFloatVectorArray & nVals,
	MFloatVectorArray & result
) {
	MStatus st;
	unsigned n = pVals.length();
	result.setLength(n);
	MFloatMatrix cameraMat;
	cameraMat.setToIdentity();
	MFloatVectorArray transparencies;
	st =  MRenderUtil::sampleShadingNetwork (
		sourceName,n,false,false,
		cameraMat,&pVals,&uVals,&vVals,&nVals,&pVals,
		0,0,0,result,transparencies
	);ert;
	return MS::kSuccess;
}

MStatus textureDeformer::sampleTexture(
	const MString & sourceName,
	MFloatPointArray & pVals,
	MFloatArray &uVals ,
	MFloatArray &vVals , 
	MFloatVectorArray & nVals,
	MFloatArray & result
) {
	MStatus st;
	unsigned n = pVals.length();
	result.setLength(n);
	MFloatMatrix cameraMat;
	cameraMat.setToIdentity();
	MFloatVectorArray transparencies;
	MFloatVectorArray colors;
	st =  MRenderUtil::sampleShadingNetwork (
		sourceName,n,false,false,
		cameraMat,&pVals,&uVals,&vVals,&nVals,&pVals,
		0,0,0,colors,transparencies
	);ert;
	for (unsigned i = 0; i < n; ++i)
	{
		result.set(colors[i].x,i);
	}
	return MS::kSuccess;
}

MStatus textureDeformer::sampleGradient(float sampleDistance, MFloatPointArray & pVals, MFloatVectorArray &result) {
	MStatus st;
	MString gradName;
	unsigned n = pVals.length();
	st = getTextureName(aGradient, gradName);ert;
	result.setLength(n);

	MFloatMatrix cameraMat;
	cameraMat.setToIdentity();


	MFloatVectorArray transparencies;
	MFloatVectorArray colors;

	MFloatPointArray pValsX(n);
	MFloatPointArray pValsY(n);
	MFloatPointArray pValsZ(n);

	MFloatVector xOffset(sampleDistance,0.0f,0.0f);
	MFloatVector yOffset(0.0f,sampleDistance,0.0f);
	MFloatVector zOffset(0.0f,0.0f,sampleDistance);

	for (unsigned i = 0;i<n;i++) {
		pValsX.set( (pVals[i]+ xOffset) , i);
		pValsY.set( (pVals[i]+ yOffset) , i);
		pValsZ.set( (pVals[i]+ zOffset) , i);
	}

	MFloatVectorArray colorsX;
	MFloatVectorArray colorsY;
	MFloatVectorArray colorsZ;
	st =  MRenderUtil::sampleShadingNetwork (gradName,n,false,false,cameraMat,&pVals,0,0,0,&pVals,0,0,0,colors,transparencies);ert;
	st =  MRenderUtil::sampleShadingNetwork (gradName,n,false,false,cameraMat,&pValsX,0,0,0,&pValsX,0,0,0,colorsX,transparencies);ert;
	st =  MRenderUtil::sampleShadingNetwork (gradName,n,false,false,cameraMat,&pValsY,0,0,0,&pValsY,0,0,0,colorsY,transparencies);ert;
	st =  MRenderUtil::sampleShadingNetwork (gradName,n,false,false,cameraMat,&pValsZ,0,0,0,&pValsZ,0,0,0,colorsZ,transparencies);ert;

	for (unsigned i = 0;i<n;i++) {
		MFloatVector r(
			((colors[i].x - colorsX[i].x) ),
			((colors[i].x - colorsY[i].x) ),
			((colors[i].x - colorsZ[i].x) )
		);
		result.set(r ,i);
	}
	return MS::kSuccess;

}


MStatus textureDeformer::deform( MDataBlock& data, MItGeometry& iter, const MMatrix& m, unsigned int multiIndex) {

	MStatus st;

	DirectionMethod dMethod = DirectionMethod(data.inputValue(aDirectionMethod).asShort());

	MObject thisNode = thisMObject();

	MPlug inPlug(thisNode,input);
	inPlug.selectAncestorLogicalIndex(multiIndex,input);
	MDataHandle hInput = data.inputValue(inPlug);
	MDataHandle hGeom = hInput.child(inputGeom); 
	MObject dGeom = hGeom.data();
	if (! dGeom.hasFn(MFn::kMesh)) {
		MGlobal::displayWarning("TextureDeformer currently works only on meshes");
		return MS::kUnknownParameter;
	}
	MItMeshVertex vIter(dGeom, &st);
	if (iter.count() != vIter.count()) {
		MGlobal::displayWarning("TextureDeformer currently works only on complete meshes");
		return MS::kUnknownParameter;
	}

	float env = data.inputValue(envelope).asFloat();	
	MFloatVector direction = data.inputValue(aDirection).asFloatVector();
	float source = data.inputValue(aSource).asFloat();
	float sampleDistance = data.inputValue(aSampleDistance).asFloat();


	MFloatMatrix cameraMat;
	cameraMat.setToIdentity();

	MFloatPointArray pVals;
	for (iter.reset(); !iter.isDone(); iter.next() ) {
		MPoint pt = iter.position()*m; 
		pVals.append(MFloatPoint(pt));
	}
///////////////////////////////////////

	unsigned n = pVals.length();
	MFloatVectorArray offset(n);
	if (dMethod == textureDeformer::kGradient) {
		st = sampleGradient(sampleDistance, pVals, offset);ert;
		for (unsigned i = 0; i < n; ++i) offset[i] = offset[i] * env;
	} else {

		MFloatVectorArray nVals;
		MFloatArray uVals;
		MFloatArray vVals;
		unsigned n = iter.count();
		// int prevIndex;
		for (vIter.reset(); !vIter.isDone(); vIter.next() ) {
			MVector norm;
			st = vIter.getNormal (norm, MSpace::kWorld);
			nVals.append(MFloatVector(norm));
			float2 uv;
			vIter.getUV(uv);
			uVals.append(uv[0]);
			vVals.append(uv[1]);
		}
		MString sourceName;
		st = getTextureName(aSource, sourceName);ert;
		MFloatArray magnitudes;
		st = sampleTexture(sourceName, pVals, uVals , vVals , nVals, magnitudes);ert;


		if (dMethod == textureDeformer::kGradientBySource) {
			st = sampleGradient(sampleDistance, pVals, offset);ert;
			for (unsigned i = 0; i < n; ++i) {
				offset[i] = offset[i].normal() * env * magnitudes[i];
			}
		} else if (dMethod == textureDeformer::kNormal)  {
			for (unsigned i = 0;i<n;i++) {	
				offset.set((nVals[i].normal() * magnitudes[i] * env) ,i);
			}
		} else if (dMethod == textureDeformer::kDirection)  {
			MString directionName;
			st = getTextureName(aDirection,directionName);
			if (! st.error()) { 
				st = sampleTexture(directionName, pVals, uVals , vVals , nVals, offset);ert;
				for (unsigned i = 0;i<n;i++) {
					offset[i] = offset[i].normal() * magnitudes[i] * env;
				}
			} else {
				for (unsigned i = 0;i<n;i++) {	
					offset[i] = direction.normal() * magnitudes[i] * env;
				}
			}

		}
	}
	unsigned i = 0;
	for ( iter.reset(); !iter.isDone(); iter.next() , i++) {
		MFloatPoint pos(pVals[i] + offset[i]);
		iter.setPosition(  MPoint(pos) * m.inverse());	
	}
	return st;
}

