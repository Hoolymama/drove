#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>

#include "errorMacros.h"

#include "texture_blend.h"
#include "wakeDeformer.h"
#include "textureDeformer.h"

MStatus initializePlugin( MObject obj)
{

	MStatus st;
	
	MString method("initializePlugin");

	MFnPlugin plugin( obj, PLUGIN_VENDOR, PLUGIN_VERSION , MAYA_VERSION);
	
	st = plugin.registerNode( "textureBlend", textureBlend::id, textureBlend::creator, textureBlend::initialize, MPxNode::kDeformerNode  );er;
	st = plugin.registerNode( "wakeDeformer", wakeDeformer::id, wakeDeformer::creator, wakeDeformer::initialize, MPxNode::kDeformerNode  );er;
	st = plugin.registerNode( "texDeformer", textureDeformer::id, textureDeformer::creator, textureDeformer::initialize, MPxNode::kDeformerNode  );er;

	MGlobal::executePythonCommand("import drove;drove.load()");

	return st;

}

MStatus uninitializePlugin( MObject obj)
{
	MStatus st;

	MString method("uninitializePlugin");

	MFnPlugin plugin( obj );
	st = plugin.deregisterNode( textureDeformer::id );
	st = plugin.deregisterNode( wakeDeformer::id );
	st = plugin.deregisterNode( textureBlend::id );

	return st;
}



