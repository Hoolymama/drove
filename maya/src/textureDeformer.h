/*
 *  textureDeformer.h
 *  jtools
 *
 *  Created by Julian Mann on 10/15/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef _textureDeformer
#define _textureDeformer




#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MTypeId.h>

#include <maya/MGlobal.h>
#include <maya/MItGeometry.h>
#include <maya/MMatrix.h>
#include <maya/MPxDeformerNode.h>
#include "lineTreeData.h"

////////////////////////////////////////////////////////////
 class textureDeformer : public MPxDeformerNode {

 public:
 	textureDeformer();
 	virtual							~textureDeformer();

 	static		void*		creator();
 	static		MStatus		initialize();

 	virtual		MStatus		deform(	MDataBlock& block,
 		MItGeometry& iter,
 		const MMatrix& mat,
 		unsigned int multiIndex);


	// void doRampLookup( const MObject& attribute, const MFloatArray& in, float mult , MFloatArray& results) const;
	// MStatus normalizeDistances(const MFloatArray& numerators, const MFloatArray& denominators,  MFloatArray& results) const;
 public:


 	static		MObject		aDirectionMethod;

 	static		MObject		aSource;

 	static		MObject		aDirection;

 	static		MObject		aGradient;

 	static		MObject		aSampleDistance;

 	static		MTypeId		id;
 private:
 	MStatus getTextureName(const MObject &attribute, MString &name);
 	MStatus sampleGradient(float sampleDistance,  MFloatPointArray & pVals, MFloatVectorArray &result) ;
 	MStatus sampleTexture(
 		const MString & sourceName,
 		MFloatPointArray & pVals,
 		MFloatArray &uVals ,
 		MFloatArray &vVals , 
 		MFloatVectorArray & nVals,
 		MFloatArray & result
 		) ;

 	MStatus sampleTexture(
 		const MString & sourceName,
 		MFloatPointArray & pVals,
 		MFloatArray &uVals ,
 		MFloatArray &vVals , 
 		MFloatVectorArray & nVals,
 		MFloatVectorArray & result
 		);
 	enum DirectionMethod { kDirection, kNormal , kGradient, kGradientBySource};


 };

#endif