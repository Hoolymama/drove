
#ifndef _textureBlend
#define _textureBlend



#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MTypeId.h>

#include <maya/MGlobal.h>
#include <maya/MItGeometry.h>
#include <maya/MItMeshVertex.h>
#include <maya/MMatrix.h>
#include <maya/MPxDeformerNode.h>
#include <maya/MFloatPointArray.h>

// #include "lineTreeData.h"

////////////////////////////////////////////////////////////
class textureBlend : public MPxDeformerNode {

public:
	textureBlend();
	virtual							~textureBlend();

	static		void*		creator();
	static		MStatus		initialize();

	virtual		MStatus		deform(	MDataBlock& block,
		MItGeometry& iter,
		const MMatrix& mat,
		unsigned int multiIndex);

	static		MObject		aTarget;
	static		MObject		aBlendValue;
	static 		MObject 	aTargetSpace;
	static		MTypeId		id;


private:
	MStatus getTextureName(const MObject &attribute, MString &name) const;

	MStatus sampleTexture(
		const MString & sourceName,
		MFloatPointArray & pVals,
		MFloatArray &uVals ,
		MFloatArray &vVals , 
		MFloatVectorArray & nVals,
		MFloatArray & result
		) const;

	// MStatus getComponentIds(MDataBlock& data, const MObject& attribute, MIntArray &result, unsigned meshSize) const;
// MStatus getComponentIds(const MObject& dComp, MIntArray &result, int vCount) const ;
MStatus componentWeights(
	const MString &mapName,
	MItMeshVertex & vIter,
	float defaultWeight,
	MFloatArray &result
) const;


 	enum TargetSpace { kObject, kWorld };



};
#endif
