/*
 *  wakeDeformer.cpp
 *  jtools
 *
 *  Created by Julian Mann on 30/07/2008.
 *  Copyright 2008 hoolyMama. All rights reserved.
 *
 */


#include <maya/MIOStream.h>

#include <maya/MFnAnimCurve.h>

#include <maya/MFnNumericAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MQuaternion.h>
#include <maya/MGlobal.h>

#include <maya/MItGeometry.h>

#include <maya/MItMeshVertex.h>
#include <maya/MFnMesh.h>
#include <maya/MItSurfaceCV.h>
#include <maya/MFnNurbsSurface.h>
#include <maya/MRenderUtil.h>

#include <maya/MPlugArray.h>
#include <maya/MFloatArray.h>
#include <maya/MVector.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatPoint.h>
#include <maya/MPoint.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatMatrix.h>

#include <maya/MRampAttribute.h>
#include <maya/MDagPath.h>
#include <maya/MMatrix.h>
#include <maya/MTypeId.h>
#include <maya/MDataBlock.h>

#include <lineData.h>
#include <lineTreeData.h>

#include <errorMacros.h>
#include <lookup.h>
#include <attrUtils.h>
#include "jMayaIds.h"

#include "wakeDeformer.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////

MTypeId	wakeDeformer::id( k_wakeDeformer );


MObject	wakeDeformer::aCurve;
MObject	wakeDeformer::aMaxDistance;

MObject	wakeDeformer::aVector;
MObject	wakeDeformer::aNormal;
MObject	wakeDeformer::aTangent;
MObject	wakeDeformer::aOrbital;

MObject	wakeDeformer::aVectorFalloff;
MObject	wakeDeformer::aNormalFalloff;
MObject	wakeDeformer::aTangentFalloff;
MObject	wakeDeformer::aOrbitalFalloff;

MObject	wakeDeformer::aVectorParamDistance;
MObject	wakeDeformer::aNormalParamDistance;
MObject	wakeDeformer::aTangentParamDistance;
MObject	wakeDeformer::aOrbitalParamDistance;

MObject	wakeDeformer::aVectorParamMag;
MObject	wakeDeformer::aNormalParamMag;
MObject	wakeDeformer::aTangentParamMag;
MObject	wakeDeformer::aOrbitalParamMag;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

wakeDeformer::wakeDeformer() {}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

wakeDeformer::~wakeDeformer() {}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void* wakeDeformer::creator() {
	return new wakeDeformer();
}




///////////////////////////////////////////////////////////////////////////////////////////////////////////////

MStatus wakeDeformer::initialize() {
	
	
	MStatus st;
	MFnNumericAttribute		nAttr;
	MFnTypedAttribute		tAttr;
	// MFnMessageAttribute	mAttr;
	// MFnEnumAttribute		eAttr;
	MFnCompoundAttribute	cAttr;
	MRampAttribute			rAttr;
	
	aCurve = tAttr.create("curve", "crv", MFnData::kDynSweptGeometry);
	tAttr.setReadable(false);
	tAttr.setCached(false);	
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute( aCurve ); er

	
	aMaxDistance = nAttr.create( "maxDistance", "md", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	nAttr.setConnectable(true);
	nAttr.setKeyable(true);	
	addAttribute( aMaxDistance );
	
	//////////////////////////////////////////
	aVector = nAttr.create("vector","vec", MFnNumericData::k3Double);
	nAttr.setHidden( false );
	nAttr.setKeyable( true );
	nAttr.setDefault( 0.0, 1.0, 0.0 );
	st = addAttribute( aVector );er;
	
	aNormal = nAttr.create( "normal", "nor", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	nAttr.setConnectable(true);
	nAttr.setKeyable(true);	
	addAttribute( aNormal );
	
	aTangent = nAttr.create( "tangent", "tan", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	nAttr.setConnectable(true);
	nAttr.setKeyable(true);	
	addAttribute( aTangent );
	
	aOrbital = nAttr.create( "orbital", "orb", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	nAttr.setConnectable(true);
	nAttr.setKeyable(true);	
	addAttribute( aOrbital );
	
	aVectorFalloff = MRampAttribute::createCurveRamp("vectorFalloff","vecf");
	st = addAttribute( aVectorFalloff );er;
	aNormalFalloff = MRampAttribute::createCurveRamp("normalFalloff","norf");
	st = addAttribute( aNormalFalloff );er;
	aTangentFalloff = MRampAttribute::createCurveRamp("tangentFalloff","tanf");
	st = addAttribute( aTangentFalloff );er;
	aOrbitalFalloff = MRampAttribute::createCurveRamp("orbitalFalloff","orbf");
	st = addAttribute( aOrbitalFalloff );er;

	aVectorParamDistance = MRampAttribute::createCurveRamp("vectorParamDistance","vecd");
	st = addAttribute( aVectorParamDistance );er;
	aNormalParamDistance = MRampAttribute::createCurveRamp("normalParamDistance","nord");
	st = addAttribute( aNormalParamDistance );er;
	aTangentParamDistance = MRampAttribute::createCurveRamp("tangentParamDistance","tand");
	st = addAttribute( aTangentParamDistance );er;
	aOrbitalParamDistance = MRampAttribute::createCurveRamp("orbitalParamDistance","orbd");
	st = addAttribute( aOrbitalParamDistance );er;
	
	aVectorParamMag = MRampAttribute::createCurveRamp("vectorParamMag","vecm");
	st = addAttribute( aVectorParamMag );er;
	aNormalParamMag = MRampAttribute::createCurveRamp("normalParamMag","norm");
	st = addAttribute( aNormalParamMag );er;
	aTangentParamMag = MRampAttribute::createCurveRamp("tangentParamMag","tanm");
	st = addAttribute( aTangentParamMag );er;
	aOrbitalParamMag = MRampAttribute::createCurveRamp("orbitalParamMag","orbm");
	st = addAttribute( aOrbitalParamMag );er;
	
	
		
	st = attributeAffects(aCurve, outputGeom ); er;

	st = attributeAffects(aMaxDistance, outputGeom ); er;
	
	st = attributeAffects(aVector, outputGeom ); er;
	st = attributeAffects(aNormal, outputGeom ); er;
	st = attributeAffects(aTangent, outputGeom ); er;
	st = attributeAffects(aOrbital, outputGeom ); er;
	
	st = attributeAffects(aVectorFalloff, outputGeom ); er;
	st = attributeAffects(aNormalFalloff, outputGeom ); er;
	st = attributeAffects(aTangentFalloff, outputGeom ); er;
	st = attributeAffects(aOrbitalFalloff, outputGeom ); er;
	
	st = attributeAffects(aVectorParamDistance, outputGeom ); er;
	st = attributeAffects(aNormalParamDistance, outputGeom ); er;
	st = attributeAffects(aTangentParamDistance, outputGeom ); er;
	st = attributeAffects(aOrbitalParamDistance, outputGeom ); er;
	
	st = attributeAffects(aVectorParamMag, outputGeom ); er;
	st = attributeAffects(aNormalParamMag, outputGeom ); er;
	st = attributeAffects(aTangentParamMag, outputGeom ); er;
	st = attributeAffects(aOrbitalParamMag, outputGeom ); er;
	
	return MStatus::kSuccess;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
void wakeDeformer::doRampLookup( const MObject& attribute, const MFloatArray& in, float mult, MFloatArray& results ) const
{
	MStatus st;
	MRampAttribute rampAttr( thisMObject(), attribute, &st ); er;
	unsigned n = in.length();
	results.setLength(n);
	for( unsigned i = 0; i < n; i++ )
	{
		float & value = results[i];
		rampAttr.getValueAtPosition( in[i], value, &st ); er;
		value *= mult;

	}
}

MStatus wakeDeformer::normalizeDistances(const MFloatArray& numerators, const MFloatArray& denominators,  MFloatArray& results) const {

	unsigned n = numerators.length();
	unsigned d = denominators.length();
	if (n != d) return MStatus::kFailure;
	results.setLength(n);
	for (unsigned i = 0; i < n; i++ ) {
		if (denominators[i] == 0) {
			results[i] = 1.0;
		} else {
			results[i] = numerators[i] /  denominators[i];
		}
	}
	return MStatus::kSuccess;
}


MStatus wakeDeformer::deform( MDataBlock& data, MItGeometry& iter, const MMatrix& m, unsigned int multiIndex) {
	
	MStatus st;

	// Get envelope float
	float env = data.inputValue(envelope).asFloat();	

	MDataHandle h = data.inputValue( aCurve, &st ); ert;	
	float maxDist = data.inputValue(aMaxDistance).asFloat(); // global max dist multiplier

	
	MVector vectorVal = data.inputValue(aVector).asVector() * double(env);
	float normalVal = data.inputValue(aNormal).asFloat() * env;
	float tangentVal = data.inputValue(aTangent).asFloat()* env;
	float orbitalVal = data.inputValue(aOrbital).asFloat()* env;
	
	

	lineTreeData * lineTree = new lineTreeData;
	lineTree->create(h);
	//cerr << "m " << m << endl;
	if (lineTree->tree()->root()) {
		// get arrays of closest point data
		///////////////////////////////////
		MIntArray indices;
		MPointArray inPoints;
		MPointArray resPoints;
		MVectorArray resTangents;
		MFloatArray resUs;
		MFloatArray resDists;
		
		bool foundAPoint;
		lineData ld;
		double searchRadius = double(maxDist);

		for (iter.reset(); !iter.isDone(); iter.next() ) {

			MPoint pt = iter.position() * m;

			foundAPoint =  lineTree->closestPointOnLineInRadius(pt,searchRadius,ld) ;
			if (foundAPoint) {
				inPoints.append(pt);
				indices.append(iter.index());
				resPoints.append(MPoint(ld.cachedPoint()));
				resTangents.append(ld.tangent());
				resUs.append(float(ld.calcU()));
				resDists.append(float(ld.cachedDist()));
			}
		}
		///////////////////////////////////
		unsigned nFound = indices.length();

		MVectorArray offsets(nFound);
		
		MFloatArray paramDists, paramMags, normalizedDists, fallOffs;
		
		if (!(vectorVal.isEquivalent(MVector::zero))) {
			// calc user vectors for found points		
			doRampLookup( aVectorParamDistance, resUs, maxDist, paramDists ); 
			doRampLookup( aVectorParamMag, resUs, 1.0f, paramMags ); 
			normalizeDistances(resDists, paramDists, normalizedDists);
			doRampLookup( aVectorFalloff, normalizedDists, 1.0f, fallOffs ); 
			for (unsigned i = 0;i < nFound; i++) {
				offsets[i] +=  double(paramMags[i] * fallOffs[i]) * vectorVal;
			}
		}
		
		if (normalVal != 0.0) {
			// calc user vectors for found points		
			doRampLookup( aNormalParamDistance, resUs, maxDist, paramDists ); 
			doRampLookup( aNormalParamMag, resUs, 1.0f, paramMags ); 
			normalizeDistances(resDists, paramDists, normalizedDists);
			doRampLookup( aNormalFalloff, normalizedDists, 1.0f, fallOffs ); 
			for (unsigned i = 0;i < nFound; i++) {
				MVector diffN = (inPoints[i] - resPoints[i]).normal();
				offsets[i] +=  double(paramMags[i] * fallOffs[i]* normalVal) * diffN;
			}
		}
		
		if (tangentVal != 0.0) {
			// calc user vectors for found points		
			doRampLookup( aTangentParamDistance, resUs, maxDist, paramDists ); 
			doRampLookup( aTangentParamMag, resUs, 1.0f, paramMags ); 
			normalizeDistances(resDists, paramDists, normalizedDists);
			doRampLookup( aTangentFalloff, normalizedDists, 1.0f, fallOffs ); 
			for (unsigned i = 0;i < nFound; i++) {
				offsets[i] +=  double(paramMags[i] * fallOffs[i]* tangentVal) * resTangents[i];
			}
		}
		
		if (orbitalVal != 0.0) {
			// calc user vectors for found points		
			doRampLookup( aOrbitalParamDistance, resUs, maxDist, paramDists ); 
			doRampLookup( aOrbitalParamMag, resUs, 1.0f, paramMags ); 
			normalizeDistances(resDists, paramDists, normalizedDists);
			doRampLookup( aOrbitalFalloff, normalizedDists, 1.0f, fallOffs ); 
			for (unsigned i = 0;i < nFound; i++) {
				MQuaternion q(  double( paramMags[i] * fallOffs[i] * orbitalVal),resTangents[i]);
				MVector diff = (inPoints[i] - resPoints[i]);
				offsets[i] += diff.rotateBy(q) - diff;
			}
		}
		
		unsigned i = 0;
		
		if (nFound > 0) {
			unsigned last = indices[(nFound - 1)];
			for (iter.reset() ; !iter.isDone(); iter.next() ) {
				if (iter.index() > last) break;
				if (iter.index() == indices[i]){
					iter.setPosition((inPoints[i]+offsets[i])* m.inverse());	
					i++;
				}
			}
		}
	}
	
	if (lineTree) {
		delete lineTree;
	}
	lineTree =0;
	
	
	return st;
}

